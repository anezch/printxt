PrintXT
=======

Print text based documents to multiple formats:

- Plain txt
- HTML
- Epson ESCP/2

## Requirements

- Python 3.7+
- Pipenv

## How To Test

- Install dependencies with `pipenv install`
- Run coverage with `coverage run -m unittest`

## How to Use

See `tests/v2/test_document.py` to see an example to using this library.
