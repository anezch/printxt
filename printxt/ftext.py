from . import textfn
from . import driver
import decimal
import textwrap

class FText(object):
    def __init__(self, text='', width=0, driver_formatter=driver.DEFAULT_TEXT_FORMATTER):
        self._text = [text]
        self._lines = 1
        self._current_line = 1
        if width > 0:
            self._text = textwrap.wrap(text, width)
        self._lines = len(self._text)
        self._driver_formatter = driver_formatter

    def get(self, line=0):
        if line > self._lines:
            raise IndexError(line)
        if line > 0:
            return self._text[line-1]
        else:
            return self._text[self._current_line-1]

    def set(self, text, line=1):
        if line > self._lines:
            raise IndexError(line)
        if line > 0:
            self._text[line-1] = text
        else:
            self._text[self._current_line-1] = text
        return self

    def linecount(self):
        return self._lines

    def nextline(self):
        if self._current_line >= self._lines:
            self._current_line = self._lines
            return
        self._current_line += 1

    def align(self, alignment, size):
        return self.set(textfn.align(alignment, self._text[0], size))

    def aright(self, size):
        return self.align('r', size)

    def aleft(self, size):
        return self.align('l', size)

    def acenter(self, size):
        return self.align('c', size)

    def repeat(self, count):
        return self.set(self.get() * count)

    def prepend(self, text):
        return self.set(''.join([text,  self.get()]))

    def append(self, text):
        return self.set(''.join([self.get(), text]))

    def join(self, textlist):
        return self.set(self.get().join(textlist))

    def monetary(self, fval):
        return self.set('{:,.2f}'.format(fval))

    def b_(self):
        return self._driver_formatter.tag_open('bold')
    def _b(self):
        return self._driver_formatter.tag_close('bold')

    def u_(self):
        return self._driver_formatter.tag_open('underline')
    def _u(self):
        return self._driver_formatter.tag_close('underline')

    def bold(self):
        return self.set(self._driver_formatter.bold(self.get()))

    def underline(self):
        return self.set(self._driver_formatter.underline(self.get()))

    def h1(self):
        return self.set(self._driver_formatter.h1(self.get()))

    def h2(self):
        return self.set(self._driver_formatter.h2(self.get()))

    def h3(self):
        return self.set(self._driver_formatter.h3(self.get()))

    def __str__(self):
        return self.get()

    def __iter__(self):
        for t in self._text:
            yield FText(t)

class FNum(object):
    def __init__(self, num):
        self._num = decimal.Decimal(str(num))

    def round2(self, scale):
        pass

    def thsep(self):
        return FText('{:,}'.format(self._num))


def ft(text='', width=0):
    return FText(text, width)
