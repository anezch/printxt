import textwrap
import functools

class GridColumn(object):
    ALIGN_MAP = {
        'l': str.ljust,
        'r': str.rjust,
        'c': str.center
    }
    def __init__(self, width=1, halign='l', valign='t', wrap=True, padding_left='', padding_right='', placeholder='...'):
        self.width = width
        self.halign = halign
        self.valign = valign
        self.wrap = wrap
        self.padding_left = padding_left
        self.padding_right = padding_right
        self.placeholder = placeholder

    def content_width(self):
        w = self.width
        if self.padding_left:
            w -= len(self.padding_left)
        if self.padding_right:
            w -= len(self.padding_right)
        return w

    def blank_line(self):
        return '{}{}{}'.format(
            self.padding_left or '',
            ' ' * self.content_width(),
            self.padding_right or ''
            )

    def render_text(self, text):
        max_lines = None
        if not self.wrap:
            max_lines = 1
        wrapped = textwrap.wrap(text,
            width=self.content_width(),
            max_lines=max_lines,
            placeholder=self.placeholder)
        aligned = []
        for l in wrapped:
            aligned_text = '{}{}{}'.format(
                self.padding_left or '',
                self.ALIGN_MAP[self.halign](l, self.content_width()),
                self.padding_right or '')
            aligned.append(aligned_text)
        return aligned

    def vertical_align(self, text_lines, height):
        res = []
        for i in range(0, height):
            if i < len(text_lines):
                res.append(text_lines[i])
            else:
                res.append(self.blank_line())
        return res


class Grid(object):
    def __init__(self, column_separator = ''):
        self.columns = []
        self.column_separator = column_separator

    def add_column(self, width=1, halign='l', valign='', wrap=True, padding_left='', padding_right='', placeholder='...'):
        col = GridColumn(width=width, halign=halign, valign=valign,
            wrap=wrap,
            padding_left=padding_left, padding_right=padding_right,
            placeholder=placeholder)
        self.columns.append(col)
        return col

    def render_row(self, col_data_list):
        index = 0
        col_lines = []
        while index < len(self.columns):
            col = self.columns[index]
            col_text = ''
            if index < len(col_data_list):
                col_text = col_data_list[index]
            cl = col.render_text(col_text)
            col_lines.append(cl)
            index += 1

        row_height = functools.reduce(lambda x, y: max(x, len(y)), col_lines, 1)
        aligned_col_lines = []
        index = 0
        while index < len(self.columns):
            col = self.columns[index]
            lines = col.vertical_align(col_lines[index], row_height)
            aligned_col_lines.append(lines)
            index += 1

        row_lines = []
        for line_index in range(0, row_height):
            line_str = ''
            col_index = 0
            while col_index < len(aligned_col_lines):
                ca = [line_str]
                if col_index > 0:
                    ca.append(self.column_separator)
                ca.append(aligned_col_lines[col_index][line_index])
                line_str = ''.join(ca)
                col_index += 1
            row_lines.append(line_str)

        return row_lines


if __name__ == '__main__':
    grid = Grid(column_separator='|')
    grid.add_column(width=40, halign='l')
    grid.add_column(width=20, halign='c')
    grid.add_column(width=20, halign='r')

    rows = [
        [
            "Wardah lipstick long lasting alpha one beta two single double",
            "Lipstick",
            "2.000.000,00"
        ],
        [
            "Make Over two way cake super light feel extra cover reddish",
            "Two Way Cake Super Light Feel Cover",
            "122.000.000.000.000,00"
        ]
    ]

    for row in rows:
        lines = grid.render_row(row)
        for line in lines:
            print(line)

