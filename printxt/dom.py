# dom.py
#
# Document model abstraction
#

import textwrap
import functools

import logging
_logger = logging.getLogger(__name__)


class Row(object):
    """
    Logical row abstraction. 1 logical row may span to multiple
    physical rows.
    """

    PHEAD = 'phead'
    PFOOT = 'pfoot'
    ROW = 'row'
    THEAD = 'shead'
    SHEAD = 'shead'
    TROW = 'row'
    TFOOT = 'sfoot'
    SFOOT = 'sfoot'

    def __init__(self, lines=None, row_type=ROW):
        """
        Initialize a Row object. row_type may be:
        Row.PHEAD: Page header
        Row.PFOOT: Page footer
        Row.THEAD: Table header
        Row.TROW: Table row
        Row.TFOOT: Table footer
        """
        if lines:
            self._lines = lines
        else:
            self._lines = []

        self.row_type = row_type

    def add_line(self, line):
        """
        Adds physical line to this logical Row
        """
        self._lines.append(line)

    def line_count(self):
        return len(self._lines)

    def has_line(self):
        return len(self._lines) > 0

    def get_line(self, line=0):
        return self._lines[line]

    def __iter__(self):
        for line in self._lines:
            yield line

class Document(object):
    def __init__(self):
        self._rows = []
        self._header_row = None
        self._footer_row = None
        self._repeat_rows = []
        self._pages = []

    def add_row(self, row):
        self._rows.append(row)
        if row.row_type == Row.PHEAD:
            self._header_row = row
        if row.row_type == Row.PFOOT:
            self._footer_row = row

    def row_count(self):
        return len(self._rows)

    def row(self, index):
        return self._rows[index]

    def create_page(self, page_format):
        page = Page(doc=self, page_format=page_format, header=self._header_row, footer=self._footer_row)
        page._page_index = len(self._pages) + 1
        self._pages.append(page)
        return page

    def page_count(self):
        return len(self._pages)

    def paginate(self, page_format):
        # Track table head to repeat if there is table that spans accross pages
        section_stack = []
        # Should be the first page here
        current_page = self.create_page(page_format)
        # Iterate each rows in document
        for row in self._rows:
            # Check if the row fits into page (take into account the header and footer)
            if not current_page.row_can_fit(row):
                # Create new page
                current_page = self.create_page(page_format)
                # Add repeated rows
                for rr in self._repeat_rows:
                    current_page.add_row(rr)
                # Put table head if we currently in the middle of a table
                if len(section_stack) > 0:
                    current_page.add_row(section_stack[len(section_stack)-1])
            # Add row to page
            if row.row_type != Row.PHEAD and row.row_type != Row.PFOOT:
                current_page.add_row(row)

            if row.row_type == Row.THEAD or row.row_type == Row.SHEAD:
                section_stack.append(row)
            if row.row_type == Row.TFOOT or row.row_type == Row.SFOOT:
                section_stack.pop()

        return self._pages

    def pages(self):
        return self._pages

    def __iter__(self):
        for page in self._pages:
            yield page

class PageFormat(object):
    def __init__(self, lines=66, top_margin=1, bottom_margin=1):
        self.lines = lines
        self.top_margin = top_margin
        self.bottom_margin = bottom_margin

    def content_line_count(self):
        return self.lines - self.top_margin - self.bottom_margin

class Page(object):
    def __init__(self, doc=None, page_format=None, header=None, footer=None, page_index=0):
        self._rows = []
        self._doc = doc
        self._page_format = page_format
        self._page_index = page_index
        self._header = header
        self._footer = footer

    def add_row(self, row):
        self._rows.append(row)

    def pop_row(self):
        return self._rows.pop(0)

    def row(self, index):
        return self._rows[index]

    def line_count(self):
        return sum(map(lambda row: row.line_count(), self._rows))

    def line_count_with_hf(self):
        count = self.line_count()
        if self._header:
            count += self._header.line_count()
        if self._footer:
            count += self._footer.line_count()
        return count

    def row_can_fit(self, row):
        final_length = self.line_count_with_hf() + row.line_count()
        return final_length <= self._page_format.content_line_count()

    def doc(self):
        return self._doc

    def __iter__(self):
        if self._header:
            yield self._header
        for row in self._rows:
            yield row
        if self._footer:
            yield self._footer

class Column(object):
    ALIGN_MAP = {
        'l': str.ljust,
        'r': str.rjust,
        'c': str.center
    }
    def __init__(self, width=1, halign='l', valign='t', wrap=True, padding_left='', padding_right='', placeholder='...'):
        self.width = width
        self.halign = halign
        self.valign = valign
        self.wrap = wrap
        self.padding_left = padding_left
        self.padding_right = padding_right
        self.placeholder = placeholder

    def content_width(self):
        w = self.width
        if self.padding_left:
            w -= len(self.padding_left)
        if self.padding_right:
            w -= len(self.padding_right)
        return w

    def blank_line(self):
        return '{}{}{}'.format(
            self.padding_left or '',
            ' ' * self.content_width(),
            self.padding_right or ''
            )

    def render_text(self, text):
        max_lines = None
        if not self.wrap:
            max_lines = 1
        wrapped = textwrap.wrap(text,
            width=self.content_width(),
            max_lines=max_lines,
            placeholder=self.placeholder,
            drop_whitespace=False)
        aligned = []
        for l in wrapped:
            aligned_text = '{}{}{}'.format(
                self.padding_left or '',
                self.ALIGN_MAP[self.halign](l, self.content_width()),
                self.padding_right or '')
            aligned.append(aligned_text)
        return aligned

    def vertical_align(self, text_lines, height):
        res = []
        for i in range(0, height):
            if i < len(text_lines):
                res.append(text_lines[i])
            else:
                res.append(self.blank_line())
        return res


class ColumnBuilder(object):
    def __init__(self, column_separator='', context={}):
        self.columns = []
        self.column_separator = column_separator
        self.context = context

    def add_column(self, width=1, halign='l', valign='', wrap=True, padding_left='', padding_right='', placeholder='...'):
        col = Column(width=width, halign=halign, valign=valign,
            wrap=wrap,
            padding_left=padding_left, padding_right=padding_right,
            placeholder=placeholder)
        self.columns.append(col)
        return col

    def render_row(self, col_data_list):
        index = 0
        col_lines = []
        while index < len(self.columns):
            col = self.columns[index]
            col_text = ''
            if index < len(col_data_list):
                col_text = col_data_list[index]['text']
            cl = col.render_text(col_text)
            col_lines.append(cl)
            index += 1

        row_height = functools.reduce(lambda x, y: max(x, len(y)), col_lines, 1)
        aligned_col_lines = []
        index = 0
        while index < len(self.columns):
            col = self.columns[index]
            lines = col.vertical_align(col_lines[index], row_height)
            aligned_col_lines.append(lines)
            index += 1

        row_lines = []
        for line_index in range(0, row_height):
            line_str = ''
            col_index = 0
            while col_index < len(aligned_col_lines):
                col_cmd = col_data_list[col_index].get('cmd', {})
                ca = [line_str]
                if col_index > 0:
                    ca.append(self.column_separator)

                aligned_col_line = aligned_col_lines[col_index][line_index]
                if col_cmd and 'apply_cmd' in self.context:
                    for cmd in col_cmd:
                        aligned_col_line = self.context['apply_cmd'](cmd, col_cmd[cmd], aligned_col_line)
                ca.append(aligned_col_line)
                line_str = ''.join(ca)
                col_index += 1
            row_lines.append(line_str)

        return Row(lines=row_lines)
