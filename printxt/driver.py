# driver.py

class DriverTextFormatter(object):
    def __init__(self, tags):
        self.TAGS = tags

    def tag_open(self, tag):
        if tag in self.TAGS:
            return self.TAGS[tag][0]
        return ''

    def tag_close(self, tag):
        if tag in self.TAGS:
            return self.TAGS[tag][1]
        return ''

    def tag_enclose(self, tag, content):
        return ''.join([self.tag_open(tag), content, self.tag_close(tag)])

    def __getattr__(self, name):
        if name in self.TAGS:
            return lambda x: self.tag_enclose(name, x)
        else:
            return lambda x: x

DEFAULT_TEXT_FORMATTER = DriverTextFormatter({})

class Plain(object):
    FORMATTING_TAGS = {
        'bold': ['', ''],
        'underline': ['', ''],
        'h1': ['', ''],
        'h2': ['', ''],
        'h3': ['', '']
    }

    def __init__(self):
        self.formatter = DriverTextFormatter(self.FORMATTING_TAGS)

    def begin_doc(self):
        return ''

    def end_doc(self):
        return ''

    def begin_page(self):
        return ''

    def end_page(self):
        return ''

    def new_line(self):
        return "\n"

def ESC(command):
    return ''.join([chr(27), command])

class ESCP(Plain):

    FORMATTING_TAGS = {
        'bold': [ESC('E'), ESC('F')],
        'underline': [ESC('-1'), ESC('-0')],
        'h1': [''.join([ESC('W1'), ESC('w1')]), ''.join([ESC('w0'), ESC('W0')])],
        'h2': [ESC('w1'), ESC('w0')],
        'h3': [ESC('W1'), ESC('W0')]
    }

    def __init__(self, page_length=66, cpi=12):
        self._page_length = page_length
        self._cpi = cpi
        super().__init__()

    def begin_doc(self):
        cc = 'M'
        if self._cpi == 10:
            cc = 'P'
        if self._cpi == 15:
            cc = 'g'
        return ''.join([chr(27), '@', chr(27), '2', chr(27), 'C', chr(self._page_length), chr(27), cc])

    def end_doc(self):
        return ''.join([chr(27), '@'])

    def end_page(self):
        return ''.join([chr(13), chr(12)])

    def new_line(self):
        return ''.join([chr(13), chr(10)])

class HTML(Plain):
    FORMATTING_TAGS = {
        'bold': ['<b>', '</b>'],
        'underline': ['<u>', '</u>'],
        'h1': ['<h1>', '</h1>'],
        'h2': ['<h2>', '</h2>'],
        'h3': ['<h3>', '</h3>']
    }

    def __init__(self, name):
        self.name = name
        super().__init__()

    def begin_doc(self):
        style = """
            @page {
                padding: 0;
                margin-left: 60px;
                margin-right: 0;
                margin-bottom: 1px;
            }

            @media print {
                html, body {
                    width: 210.1mm;
                    height: 296.9mm;
                    margin: 0;
                    /* change the margins as you want them to be. */
                }

                .header {
                    margin: auto;
                }
            }
        """
        return '<html><head><style>{}</style><title>{}</title></head><body onload="window.print()"><pre>'.format(style, self.name)

    def end_doc(self):
        return '</pre></body></html>'
    
    def end_page(self):
        return '<div style="page-break-after: always;"></div>'

    def new_line(self):
        return '<br>'
