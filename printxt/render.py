from mako.template import Template
from mako.runtime import Context
from io import StringIO
from . import ftext
from . import dom
import os
import re
import json
import datetime

import logging
_logger = logging.getLogger(__name__)

class TemplateRenderer(object):
    def __init__(self, template_file, driver):
        self._template_file = template_file
        self._driver = driver
        self._ft = lambda t='', w=0: ftext.FText(t, w, self._driver.formatter)

    def _build_content(self, data):
        template = Template(filename=self._template_file)
        buf = StringIO()
        context = Context(buf, o=data, ft=self._ft, drv=self._driver, datetime=datetime.datetime.now())
        template.render_context(context)
        return buf

    def _apply_cmd(self, cmd, cmdarg, text):
        res = text
        if not res.strip():
            return res
        if cmd == 'style':
            styles = cmdarg.split(',')
            for style in styles:
                res = self._driver.formatter.tag_enclose(style, res)
        return res

    def _build_document(self, string_buffer):
        regex = re.compile('([A-Z]+):([^>]*)>(.*)')
        row_type_map = {
            'PH': dom.Row.PHEAD, 'PF': dom.Row.PFOOT,
            'RR': dom.Row.ROW, 'CL': '--',
            'TH': dom.Row.THEAD, 'TR': dom.Row.TROW, 'TF': dom.Row.TFOOT,
            'CH': dom.Row.SHEAD, 'CF': dom.Row.SFOOT
        }
        doc = dom.Document()
        current_row = None
        string_buffer.seek(0)
        columnBuilder = None
        rowBuffer = []
        for line in string_buffer.readlines():
            m = regex.match(line)
            if m:
                line_type = m.group(1)
                line_cmd = m.group(2)
                line_str = m.group(3)

                if line_type == 'CV':
                    cbparams = {}
                    if line_str:
                        cbparams = json.loads(line_str)
                    cbparams['context'] = {
                        'apply_cmd': self._apply_cmd
                    }
                    columnBuilder = dom.ColumnBuilder(**cbparams)
                    continue

                if line_type == 'CC':
                    if not columnBuilder:
                        continue
                    ccparams = {}
                    if line_str:
                        ccparams = json.loads(line_str)
                        columnBuilder.add_column(**ccparams)
                    continue

                if line_type == 'CR':
                    rowBuffer = []
                    continue

                if line_type == 'CE':
                    if len(rowBuffer)>0 and columnBuilder:
                        doc.add_row(columnBuilder.render_row(rowBuffer))
                    rowBuffer = []
                    continue

                if line_type == 'CD':
                    cmd = {}
                    if line_cmd:
                        cmd = json.loads(line_cmd, strict=False)
                    rowBuffer.append({'cmd': cmd, 'text':line_str})
                    continue


                if line_type == 'CH':
                    row_type = dom.Row.SHEAD

                if line_type == 'CF':
                    row_type = dom.Row.SFOOT

                row_type = row_type_map[line_type]
                new_row = True

                if line_type == 'CL':
                    new_row = False

                if new_row:
                    current_row = dom.Row(row_type=row_type)
                    doc.add_row(current_row)

                current_row.add_line(line_str)
        return doc

    def _render_to_pages(self, doc, page_format):
        pages = doc.paginate(page_format)
        doc_buf = StringIO()
        doc_buf.write(self._driver.begin_doc())
        for page in pages:
            page_buf_src = StringIO()
            page_buf_src.write(self._driver.begin_page())
            page_lines = page.line_count_with_hf()
            nr = 0
            for row in page:
                for line in row:
                    page_buf_src.write(line)
                    nr += 1
                    if nr < page_lines:
                        page_buf_src.write(self._driver.new_line())
            page_buf_src.write(self._driver.end_page())
            page_template = Template(page_buf_src.getvalue())
            page_buf_dest = StringIO()
            page_context = Context(page_buf_dest, page_number=page._page_index, page_count=page._doc.page_count())
            page_template.render_context(page_context)
            doc_buf.write(page_buf_dest.getvalue())
            page_buf_src.close()
            page_buf_dest.close()
        return doc_buf

    def render(self, data, page_format):
        pages_buf = self._render_to_pages(
            self._build_document(
                self._build_content(data)
            ),
            page_format
        )
        return pages_buf

