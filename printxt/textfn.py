
def align(align, text, size):
    sym = '<'
    if align == 'r':
        sym = '>'
    if align == 'c':
        sym = '^'
    return '{:{sym}{width}}'.format(text, sym=sym, width=size)

def center(text, size):
    return align('c', text, size)

def left(text, size):
    return align('l', text, size)

def right(text, size):
    return align('r', text, size)

def trunc(text, size):
    return '{:.{width}}'.format(text, width=size)

def rept(text, size):
    return text * size
