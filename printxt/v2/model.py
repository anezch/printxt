"""
Document model for PrintXT
"""
from __future__ import annotations
from typing import Optional, Any, List
import textwrap
import functools

from .interface import AbstractStyle
from .document_driver import AbstractDocumentDriver, driver_registry

class Style(AbstractStyle):
    """
    Class Style implements AbstractStyle interface. This class is used
    to give styling info to a Column
    """
    def __init__(self, code: str = '') -> None:
        self.with_code(code)

    def with_code(self, code: str) -> Style:
        """
        Alter the styling info with code string. A character in the code string
        activates these styles respectively:
        - h: heading
        - b: bold
        - u: underline
        - i: italic
        """
        self.heading: bool = code.find('h') != -1
        self.bold: bool = code.find('b') != -1
        self.underline: bool = code.find('u') != -1
        self.italic: bool = code.find('i') != -1
        return self

    def is_heading(self):
        return self.heading

    def is_bold(self):
        return self.bold

    def is_underline(self):
        return self.underline

    def is_italic(self):
        return self.italic

class Column:
    """
    Class Column to model a column of text
    """
    def __init__(self,
            parent_row: Row,
            width: int = 0,
            h_align: str = 'l',
            v_align: str = 't',
            style: Optional[AbstractStyle] = None,
            style_code: str = None) -> None:
        self.parent_row: Row = parent_row
        self.h_align: str = h_align
        self.v_align: str = v_align
        self.width: int = width
        self.style: Optional[AbstractStyle] = style
        if style_code:
            self.style = Style(code = style_code)
        self.lines: List[str] = []
        self.render_cache: List[str] = []

    def set_style_code(self, style_code: str) -> Column:
        """
        Sets column style with a code string
        """
        self.style = Style(code = style_code)
        return self

    def set_width(self, width: int) -> Column:
        """
        Sets the column width in character
        """
        self.width = width
        return self

    def set_align(self, h_align: str, v_align: str) -> Column:
        """
        Sets horizontal and vertical alignment of this Column
        """
        self.h_align = h_align
        self.v_align = v_align
        return self

    def set_h_align(self, h_align: str) -> Column:
        """
        Sets horizontal alignment of this Column
        """
        self.h_align = h_align
        return self

    def set_v_align(self, v_align: str) -> Column:
        """
        Sets vertical alignment of this Column
        """
        self.v_align = v_align
        return self

    def add_line(self, line: str) -> Column:
        """
        Add a line of text to this Column.
        Note that the line will be split by newline character, so
        this could end up adding multiple lines if the string supplied
        has newlines in it.
        """
        self.lines.extend(line.splitlines())
        return self

    def add_lines(self, lines: List[str]) -> Column:
        """
        Add a number of lines to this Column.
        Note that each line will be split by newline character, so
        this could end up adding more lines if the strings supplied
        has newlines in them.
        """
        for line in lines:
            self.lines.extend(line.splitlines())
        return self

    def line_count(self) -> int:
        """
        Returns the number of text lines in this Column
        """
        return len(self.lines)

    def create_sibling(self) -> Column:
        """
        Creates a sibling Column that has the same parent Row
        as this Column. This will throw an exception if this Column
        has no Row parent.
        """
        return self.parent_row.create_column()

    def new_row(self) -> Row:
        """
        Creates a new Row that is a sibling to this Column's parent Row.
        This is a convenience method to start a new row when chaining.
        """
        return self.parent_row.create_sibling()

    def get_document_driver(self) -> Optional[AbstractDocumentDriver]:
        """
        Gets the document driver used by the ancestor (Row, Section, Document)
        """
        if self.parent_row:
            return self.parent_row.get_document_driver()
        return None

    def get_available_width(self) -> int:
        """
        Gets available width. Since heading style could be using more than 1
        character space, this function will take that into account. For example
        if this Column is a heading and has width of 40 characters, and the driver
        states that a heading consumes 2 characters space, the avialable width
        will be 40/2 = 20.
        """
        driver: Optional[AbstractDocumentDriver] = self.get_document_driver()
        if driver and self.style and self.style.is_heading():
            return int(self.width / driver.heading_width_factor())
        return self.width

    def __h_align(self, text: str, width: int) -> str:
        """
        Private helper for horizontal text alignment
        """
        if self.h_align == 'l':
            return str.ljust(text, width)
        elif self.h_align == 'r':
            return str.rjust(text, width)
        elif self.h_align == 'c':
            return str.center(text, width)
        return str.ljust(text, width)

    def render_to_cache(self) -> None:
        """
        Renders the Column to actual text rendering and store the rendering
        to instance's cache variable.
        Some information only available after render cache is calculated:
        
        - rendered_length
        - get_rendered_line

        Any change to this column content and/or attributes would require
        the render cache to be before calling above functions.
        """
        available_width: int = self.get_available_width()

        self.render_cache = []
        for line in self.lines:
            wrapped = textwrap.wrap(line,
                    width = available_width,
                    drop_whitespace=True)
            for wrapped_line in wrapped:
                self.render_cache.append(self.__h_align(wrapped_line, available_width))

    def rendered_length(self) -> int:
        """
        Gets the physical length consumption of this Column.
        This information only valid after render cache is built with render_to_cache.
        This function takes into account vertical space multiplier if this Column
        is styled as a heading.
        """
        driver: Optional[AbstractDocumentDriver] = self.get_document_driver()
        if driver and self.style and self.style.is_heading():
            return len(self.render_cache) * driver.heading_height_factor()
        return len(self.render_cache)

    def get_rendered_line(self, line_number: int, vert_space: int = -1) -> str:
        """
        Gets a line from the render cache.
        The line number could be greater than the actual number of render cache
        lines. This is for situation where there are Column sibling that has
        greater number of render lines. In this case, this function will take
        into account the Column's vertical alignment.
        """
        if not self.render_cache:
            self.render_to_cache()

        driver: Optional[AbstractDocumentDriver] = self.get_document_driver()
        real_width: int = self.width
        if driver and self.style and self.style.is_heading():
            real_width = int(real_width / driver.heading_width_factor())

        line_count = len(self.render_cache)

        if vert_space > line_count:
            if self.v_align == 't':
                if line_number < line_count:
                    return self.render_cache[line_number]
                return ' ' * real_width
            if self.v_align == 'b':
                offset = vert_space - line_count
                if line_number < offset:
                    return ' ' * real_width
                return self.render_cache[line_number - offset]
            if self.v_align == 'c':
                offset = int((vert_space - line_count) / 2)
                if line_number < offset:
                    return ' ' * real_width
                if (line_number - offset) < line_count:
                    return self.render_cache[line_number - offset]
                return ' ' * real_width
        elif vert_space < line_count:
            if self.v_align == 't':
                return self.render_cache[line_number]
            if self.v_align == 'b':
                return self.render_cache[line_number + (line_count - vert_space)]
            if self.v_align == 'c':
                return self.render_cache[line_number + int((line_count - vert_space)/2)]

        return self.render_cache[line_number]

class StyleInfo:
    """
    Data class to hold styling information for a chunk of text
    """
    def __init__(self, start: int, length: int, style: AbstractStyle) -> None:
        self.start: int = start
        self.length: int = length
        self.style: AbstractStyle = style

class RenderLine:
    """
    Class to model a rendered line of text.
    A render line consist of the actual text string and optionally
    a couple of StyleInfo to stylize chunks of the text
    """
    def __init__(self) -> None:
        self.line_text: str = ''
        self.style_infos: List[StyleInfo] = []

    def add_style_info(self, start: int, length: int, style: AbstractStyle) -> None:
        """
        Add style info to a chunk of the text
        """
        self.style_infos.append(StyleInfo(start, length, style))

    def stylize(self, driver: AbstractDocumentDriver) -> str:
        """
        Apply style to text chunks with the help from the Document Driver
        """
        sorted_info: List[StyleInfo] = sorted(self.style_infos, key = lambda e: e.start)

        chunks: List[str] = []
        start_index = 0
        for info in sorted_info:
            if info.start > start_index:
                chunks.append(self.line_text[start_index:info.start])
            stylized_text = self.line_text[info.start:info.start+info.length]
            chunks.append(driver.apply_style(stylized_text, info.style))
            start_index = info.start+info.length

        if start_index < len(self.line_text):
            chunks.append(self.line_text[start_index:])
        return ''.join(chunks)

class Row:
    """
    Class to model a logical Row of text.
    A Row contains one or more Columns, and will renders as one or more physical
    text rendering. These physical text rendering cannot be broken by page breaks.
    The policy that you can see in the Page class is that when a section (contains
    one or more rows) will be checked before added to a page. If there is no enough
    room in the page, it will trigger a page break and the section will be added
    to the new page.
    """
    def __init__(self, parent_section: Section, column_separator: str = '') -> None:
        self.parent_section: Section = parent_section
        self.column_separator = column_separator
        self.columns: List[Column] = []
        self.render_lines: List[RenderLine] = []
        self.content_length: int = 0

    def set_column_separator(self, separator: str) -> Row:
        """
        Sets the column separator of this Row
        """
        self.column_separator = separator
        return self

    def create_column(self) -> Column:
        """
        Creates a child column in this Row
        """
        column: Column = Column(self)
        self.columns.append(column)
        return column

    def column_count(self) -> int:
        """
        Returns the number of child columns
        """
        return len(self.columns)

    def create_sibling(self) -> Row:
        """
        A convenience method to create a sibling row, i.e. a new row that is
        the child of the same parent Section.
        """
        return self.parent_section.create_row()

    def get_document_driver(self) -> Optional[AbstractDocumentDriver]:
        """
        Gets document driver of the ancestor (Section, Document)
        """
        if self.parent_section:
            return self.parent_section.get_document_driver()
        return None

    def build_render_cache(self) -> None:
        """
        Build render cache of child Columns.
        This is required as some calculation only valid after render cache of the Columns
        are built.
        """
        for column in self.columns:
            column.render_to_cache()
        self.content_length = functools.reduce(lambda m, c: max(m, c.rendered_length()), self.columns, 0)

    def length(self) -> int:
        """
        Gets the physical rendering length of this Row. This only valid after build_render_cache()
        is called. This will be equal to the greatest children of Rows rendered length
        """
        return self.content_length

    def render(self) -> list[RenderLine]:
        """
        Returns a list of RenderLines that represents each line of physical rendered
        lines. Each RenderLine will contains the rendered line text and styling infos.
        """
        row_height = 0
        for column in self.columns:
            column.render_to_cache()
            column_lines = len(column.render_cache)
            if column_lines > row_height:
                row_height = column_lines

        self.render_lines = []
        for line_index in range(0, row_height):
            render_line: RenderLine = RenderLine()
            separator: str = ''
            for column in self.columns:
                col_text = column.get_rendered_line(line_index, vert_space=row_height)
                render_line.line_text = render_line.line_text + separator
                if column.style:
                    render_line.add_style_info(len(render_line.line_text),
                            len(col_text),
                            column.style)
                render_line.line_text = render_line.line_text + col_text
                separator = self.column_separator

            self.render_lines.append(render_line)

        return self.render_lines

class Section:
    """
    Class to model a logical Section of a document.
    A section is a group of Rows.
    """
    def __init__(self, parent_doc: Document) -> None:
        self.parent_doc: Document = parent_doc
        self.rows: List[Row] = []

    def create_row(self) -> Row:
        """
        Creates a child Row in this Section
        """
        row: Row = Row(self)
        self.rows.append(row)
        return row

    def create_single_column_row(self,
            lines: List[str],
            style_code: str = '',
            h_align: str = 'l',
            v_align: str = 't') -> Section:
        """
        A convenience method to create a single column Row
        """
        (self.create_row()
                .create_column()
                    .set_width(self.parent_doc.page_width)
                    .set_style_code(style_code)
                    .set_h_align(h_align)
                    .set_v_align(v_align)
                    .add_lines(lines))
        return self

    def row_count(self) -> int:
        """
        Returns the number of Rows in this Section
        """
        return len(self.rows)

    def get_document_driver(self) -> Optional[AbstractDocumentDriver]:
        """
        Gets the document driver of the ancestor (Document)
        """
        if self.parent_doc:
            return self.parent_doc.doc_driver
        return None

    def build_render_cache(self) -> None:
        """
        Builds render cache for this Section
        """
        for row in self.rows:
            row.build_render_cache()

    def length(self) -> int:
        """
        Returns the total physical length of this Section
        """
        return functools.reduce(lambda a, r: a + r.length(), self.rows, 0)

    def render(self) -> list[RenderLine]:
        """
        Returns render lines of this section
        """
        lines: List[RenderLine] = []
        for row in self.rows:
            lines.extend(row.render())
        return lines

class Page:
    """
    Page class to model a logical page
    """
    def __init__(self, parent_doc: Document) -> None:
        self.parent_doc: Document = parent_doc
        self.sections: List[Section] = []
        self.render_lines: List[str] = []

    def length(self) -> int:
        """
        Returns the physical length of this page
        """
        return functools.reduce(lambda a, s: a + s.length(), self.sections, 0)

    def add_section(self, section: Section) -> bool:
        """
        Add section to page.
        The policy is to make sure that a Section must fit into a page.
        If the to be added section does not fit into the page, it must
        be added to new page.
        """
        if (self.length() + section.length()) > self.parent_doc.page_length:
            return False
        self.sections.append(section)
        return True

    def section_count(self) -> int:
        """
        Returns the number of sections in this page
        """
        return len(self.sections)

class Document:
    """
    Document class to model a logical document
    """
    def __init__(self,
            doc_format: str,
            page_width: int,
            page_length: int) -> None:
        self.doc_format: str = doc_format
        self.doc_driver: AbstractDocumentDriver = driver_registry[doc_format]
        self.page_width: int = page_width
        self.page_length: int = page_length
        self.sections: List[Section] = []
        self.pages: List[Page] = []

    def create_section(self) -> Section:
        """
        Create a section inside this document
        """
        section: Section = Section(self)
        self.sections.append(section)
        return section

    def section_count(self) -> int:
        """
        Returns the number of sections in this document
        """
        return len(self.sections)

    def add_page(self) -> Page:
        """
        Add new page to this document
        """
        page: Page = Page(self)
        self.pages.append(page)
        return page

    def page_count(self) -> int:
        """
        Returns the number of pages in this document
        """
        return len(self.pages)

    def paginate(self) -> None:
        """
        Paginate the content of this document.
        Sections are laid out to pages without breaking them.
        """
        self.pages = []

        for section in self.sections:
            section.build_render_cache()

        current_page: Page = self.add_page()
        for section in self.sections:
            if not current_page.add_section(section):
                current_page = self.add_page()
                current_page.add_section(section)

    def set_driver_option(self, key: str, value: Any) -> None:
        """
        Set a driver option
        """
        if self.doc_driver:
            self.doc_driver.set_option(key, value)

    def get_driver_option(self, key: str, default: Any) -> Any:
        """
        Get a driver option
        """
        if self.doc_driver:
            return self.doc_driver.get_option(key, default)
        return default

    def render(self, writable, driver: Optional[AbstractDocumentDriver] = None) -> None:
        """
        Render the document to a writable destination, optionally using
        different driver than the one is currently set in this document object
        """
        old_driver = None
        if driver:
            old_driver = self.doc_driver
            self.doc_driver = driver

        self.set_driver_option('page_length', self.page_length)
        writable.write(self.doc_driver.begin())
        for page in self.pages:
            for section in page.sections:
                for line in section.render():
                    writable.write(line.stylize(self.doc_driver))
                    writable.write(self.doc_driver.new_line())
            writable.write(self.doc_driver.page_break())
        writable.write(self.doc_driver.end())
        writable.flush()

        if old_driver:
            self.doc_driver = old_driver

