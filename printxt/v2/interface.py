"""
Interfaces
"""
from abc import ABC, abstractmethod

class AbstractStyle(ABC):
    """
    Abstract style class
    """
    @abstractmethod
    def is_heading(self) -> bool:
        """
        Returns True if this style has heading
        """

    @abstractmethod
    def is_bold(self) -> bool:
        """
        Returns True if this style has bold
        """

    @abstractmethod
    def is_underline(self) -> bool:
        """
        Returns True if this style has underline
        """

    @abstractmethod
    def is_italic(self) -> bool:
        """
        Returns True if this style has italic
        """
