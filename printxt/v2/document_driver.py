"""
document_driver.py

This module defines classes for text document drivers
"""
from abc import ABC, abstractmethod
from typing import Any, Dict
from .interface import AbstractStyle

class AbstractDocumentDriver(ABC):
    """
    Abstract class for document driver
    """

    def __init__(self):
        self.options: Dict[str, Any] = {}

    def set_option(self, key: str, value: Any) -> None:
        """
        Set an option for this driver
        """
        self.options[key] = value

    def get_option(self, key: str, default: Any) -> Any:
        """
        Get an option for this driver with default
        """
        return self.options.get(key, default)

    @abstractmethod
    def begin(self) -> str:
        """
        Start a document. This method is called at the
        beginning of writing to output and is used to initialize
        the output.
        """

    @abstractmethod
    def end(self) -> str:
        """
        End a document. This method is called at the end of
        writing, and is used to properly close and finalize
        the output.
        """

    @abstractmethod
    def heading_width_factor(self) -> int:
        """
        Get width multiplier factor for heading text
        """

    @abstractmethod
    def heading_height_factor(self) -> int:
        """
        Get height multiplier factor for heading text
        """

    @abstractmethod
    def apply_style(self, text: str, style: AbstractStyle) -> str:
        """
        Apply style to a string
        """

    @abstractmethod
    def new_line(self) -> str:
        """
        Get new line control sequence
        """

    @abstractmethod
    def page_break(self) -> str:
        """
        Get page break control sequence
        """

driver_registry: Dict[str, AbstractDocumentDriver] = {}


class TXTDriver(AbstractDocumentDriver):
    """
    Plain text file driver
    """
    def begin(self) -> str:
        return ''

    def end(self) -> str:
        return ''

    def heading_width_factor(self) -> int:
        return 1

    def heading_height_factor(self) -> int:
        return 1

    def apply_style(self, text: str, style: AbstractStyle) -> str:
        return text

    def new_line(self) -> str:
        return '\n'

    def page_break(self) -> str:
        return '\n    ----8<---- PAGE BREAK ----8<----\n'

driver_registry['txt'] = TXTDriver()


class HTMLDriver(AbstractDocumentDriver):
    """
    HTML document driver
    """
    def begin(self) -> str:
        return f"""<html><head><title>{self.get_option('title', 'PrintXT')}</title><style>
            pre {{
              font-family: 'Ubuntu Mono',Courier,monospace;
              font-size: 12pt;
            }}
            .pt-heading {{
              font-style: bold;
              font-size: 24pt;
            }}
            .pt-bold {{
              font-weight: bold;
            }}
            .pt-underline {{
              text-decoration: underline;
            }}
            .pt-italic {{
              font-style: italic;
            }}
        </style></head><body><pre>"""

    def end(self) -> str:
        return '</pre></body></html>\n'

    def heading_width_factor(self) -> int:
        return 2

    def heading_height_factor(self) -> int:
        return 2

    def apply_style(self, text: str, style: AbstractStyle) -> str:
        classes = [c[0] for c in [
                ('pt-heading', style.is_heading()),
                ('pt-bold', style.is_bold()),
                ('pt-underline', style.is_underline()),
                ('pt-italic', style.is_italic())] if c[1]]
        if not classes:
            return text
        return f'<span class="{" ".join(classes)}">{text}</span>'

    def new_line(self) -> str:
        return '\n'

    def page_break(self) -> str:
        return '<div style="page-break-after: always;"></div>'

driver_registry['html'] = HTMLDriver()

class ESCP2Driver(AbstractDocumentDriver):
    """
    ESC/P2 document driver
    """
    def begin(self) -> str:
        cpi: int = self.get_option('escp_cpi', 12)
        cpi_code: str = 'M'
        if cpi == 10:
            cpi_code = 'P'
        if cpi == 15:
            cpi_code = 'g'

        # The default page length is based on 8.5 x 11 inch Letter
        return f'\x1b@\x1b2\x1bC{chr(self.get_option("page_length", 64))}\x1b{cpi_code}'


    def end(self) -> str:
        return '\x1b@'

    def heading_width_factor(self) -> int:
        return 2

    def heading_height_factor(self) -> int:
        return 2

    def apply_style(self, text: str, style: AbstractStyle) -> str:
        result: str = text
        if style.is_heading():
            result = f'\x1bW1\x1bw1{result}\x1bw0\x1bW0'
        if style.is_bold():
            result = f'\x1bE{result}\x1bF'
        if style.is_underline():
            result = f'\x1b-1{result}\x1b-0'
        if style.is_italic():
            result = f'\x1b4{result}\x1b5'

        return result

    def new_line(self) -> str:
        return '\x0d\x0a'

    def page_break(self) -> str:
        return '\x0d\x0c'

driver_registry['escp2'] = ESCP2Driver()
