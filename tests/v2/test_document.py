import unittest
import os
import json
from typing import List, Optional, Iterator
import difflib

from printxt.v2.model import Document, Section, Row
from printxt.v2.document_driver import driver_registry

class PTTestDocument(unittest.TestCase):

    def setUp(self):
        with open(os.path.join(os.path.dirname(__file__), 'doc_data.json')) as fp:
            self.data = json.load(fp)

    def diff_file(self, file_name1, file_name2) -> None:
        diff: Optional[Iterator[str]] = None
        with (open(file_name1, 'rt', encoding='ascii') as file_1,
                open(file_name2, 'rt', encoding='ascii') as file_2):
            lines1: List[str] = []
            lines2: List[str] = []
            for line in file_1:
                lines1.append(line)
            for line in file_2:
                lines2.append(line)
            self.assertEqual(len(lines1), len(lines2))
            diff = difflib.unified_diff(lines1, lines2, file_name1, file_name2)

        self.assertNotEqual(None, diff)
        diff_lines: List[str] = [l.strip() for l in diff]
        self.assertEqual(0, len(diff_lines), msg='\n'.join(diff_lines))

    def test_doc1(self):
        page_width = 96
        page_length = 32
        document: Document = Document('txt',
                page_width = page_width,
                page_length = page_length)

        self.assertEqual(page_width, document.page_width)
        self.assertEqual(page_length, document.page_length)

        header_section: Section = document.create_section()
        (header_section.create_row()
            .set_column_separator(' ')
            .create_column()
                .set_width(53)
                .add_line(self.data['company_name'])
                .add_line(self.data['company_address'])
            .create_sibling()
                .set_width(42)
                .add_line('Shipping address:')
                .add_line(self.data['customer_name'])
                .add_line(self.data['customer_address']))

        self.assertEqual(1, len(document.sections))

        (document.create_section()
                .create_single_column_row([f'DELIVERY ORDER {self.data["doc_number"]}'],
                    style_code='hb',
                    h_align='c'))
        
        self.assertEqual(2, len(document.sections))

        title_section: Section = document.create_section()
        (title_section.create_row()
                    .set_column_separator(' ')
                    .create_column()
                        .set_width(10)
                        .set_h_align('r')
                        .set_style_code('b')
                        .add_line('Order Ref:')
                    .create_sibling()
                        .set_width(40)
                        .set_style_code('b')
                        .add_line(self.data['order_ref'])
                    .create_sibling()
                        .set_width(9)
                        .set_h_align('r')
                        .add_line('Ship Ref:')
                    .create_sibling()
                        .set_width(34)
                        .add_line(self.data['ship_ref'])
                .new_row()
                    .set_column_separator(' ')
                    .create_column()
                        .set_width(10)
                        .set_h_align('r')
                        .add_line('Ship Date:')
                    .create_sibling()
                        .set_width(40)
                        .add_line(self.data['ship_date'])
                    .create_sibling()
                        .set_width(9)
                        .set_h_align('r')
                        .add_line('Carrier:')
                    .create_sibling()
                        .set_width(34)
                        .add_line(self.data['ship_carrier'])
                    )

        self.assertEqual(3, document.section_count())
        self.assertEqual(2, document.sections[2].row_count())
        self.assertEqual(4, document.sections[2].rows[0].column_count())
        self.assertEqual(4, document.sections[2].rows[1].column_count())


        thead_section: Section = document.create_section()
        (thead_section.create_row()
                .set_column_separator('')
                .create_column()
                    .set_width(page_width)
                    .add_line('=' * page_width)
            .new_row()
                .set_column_separator(' ')
                .create_column()
                    .set_width(75)
                    .add_line('Product')
                .create_sibling()
                    .set_width(20)
                    .set_h_align('r')
                    .add_line('Quantity')
            .new_row()
                .set_column_separator(' ')
                .create_column()
                    .set_width(page_width)
                    .add_line('=' * page_width))

        self.assertEqual(4, document.section_count())
        self.assertEqual(3, document.sections[3].row_count())
        self.assertEqual(1, document.sections[3].rows[0].column_count())
        self.assertEqual(2, document.sections[3].rows[1].column_count())
        self.assertEqual(1, document.sections[3].rows[2].column_count())

        section_counter: int = 4
        for line in enumerate(self.data['lines']):
            line_style_code: str = ''
            if line[0] % 4 == 3 and line[0] != len(self.data['lines'])-1:
                line_style_code = 'u'
            (document.create_section().create_row()
                    .set_column_separator(' ')
                    .create_column()
                        .set_width(75)
                        .set_style_code(line_style_code)
                        .add_line(line[1]['description'])
                    .create_sibling()
                        .set_width(20)
                        .set_style_code(line_style_code)
                        .set_h_align('r')
                        .add_line(str(line[1]['qty'])))
            section_counter = section_counter + 1

        self.assertEqual(section_counter, document.section_count())

        (document.create_section()
                .create_single_column_row(['=' * page_width])
                .create_single_column_row(['Notes:'])
                .create_single_column_row([self.data['note']])
                .create_single_column_row(['=' * page_width]))

        section_counter = section_counter + 1
        self.assertEqual(section_counter, document.section_count())
        self.assertEqual(4, document.sections[section_counter-1].row_count())
        self.assertEqual(1, document.sections[section_counter-1].rows[0].column_count())
        self.assertEqual(page_width, document.sections[section_counter-1].rows[0].columns[0].width)

        (document.create_section()
                .create_single_column_row([
                    'Received By:                Date:                       Delivered By:'
                ]).create_single_column_row([
                    '  Signature:                                               Signature:'
                ]).create_single_column_row([
                    ' '
                ]).create_single_column_row([
                    '            ____________________                                     ____________________']))

        section_counter = section_counter + 1
        self.assertEqual(section_counter, document.section_count())
        self.assertEqual(4, document.sections[section_counter-1].row_count())
        self.assertEqual(1, document.sections[section_counter-1].rows[0].column_count())
        self.assertEqual(page_width, document.sections[section_counter-1].rows[0].columns[0].width)

        with open('/tmp/output.txt', 'wt', encoding='ascii') as outf:
            document.paginate()
            document.render(outf)
        self.diff_file(
                os.path.join(os.path.dirname(__file__), 'test_document.txt'),
                '/tmp/output.txt')

        with open('/tmp/output.html', 'wt', encoding='ascii') as outf:
            document.doc_driver = driver_registry['html']
            document.set_driver_option('title', self.data['doc_number'])
            document.paginate()
            document.render(outf)
        self.diff_file(
                os.path.join(os.path.dirname(__file__), 'test_document.html'),
                '/tmp/output.html')

        with open('/tmp/output.prn', 'wt', encoding='ascii') as outf:
            document.doc_driver = driver_registry['escp2']
            document.set_driver_option('escp_cpi', 12)
            document.paginate()
            document.render(outf)
