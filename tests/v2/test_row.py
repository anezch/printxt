import unittest
from printxt.v2.model import Column, Row

TEST_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

ROW_RENDER_1 = """|  Lorem ipsum dolor sit amet, consectetur | Lorem ipsum dolor sit amet, consectetur  |
|   adipiscing elit, sed do eiusmod tempor | adipiscing elit, sed do eiusmod tempor   |
|     incididunt ut labore et dolore magna | incididunt ut labore et dolore magna     |
|    aliqua. Ut enim ad minim veniam, quis | aliqua. Ut enim ad minim veniam, quis    |
|     nostrud exercitation ullamco laboris | nostrud exercitation ullamco laboris     |
| nisi ut aliquip ex ea commodo consequat. | nisi ut aliquip ex ea commodo consequat. |
|   Duis aute irure dolor in reprehenderit | Duis aute irure dolor in reprehenderit   |
| in voluptate velit esse cillum dolore eu | in voluptate velit esse cillum dolore eu |
|    fugiat nulla pariatur. Excepteur sint | fugiat nulla pariatur. Excepteur sint    |
| occaecat cupidatat non proident, sunt in | occaecat cupidatat non proident, sunt in |
|   culpa qui officia deserunt mollit anim | culpa qui officia deserunt mollit anim   |
|                          id est laborum. | id est laborum.                          |"""

ROW_RENDER_2 = """ Lorem ipsum dolor sit amet, consectetur|Lorem ipsum dolor sit amet, consectetur 
  adipiscing elit, sed do eiusmod tempor|adipiscing elit, sed do eiusmod tempor  
    incididunt ut labore et dolore magna|incididunt ut labore et dolore magna    
   aliqua. Ut enim ad minim veniam, quis|aliqua. Ut enim ad minim veniam, quis   
    nostrud exercitation ullamco laboris|nostrud exercitation ullamco laboris    
nisi ut aliquip ex ea commodo consequat.|nisi ut aliquip ex ea commodo consequat.
  Duis aute irure dolor in reprehenderit|Duis aute irure dolor in reprehenderit  
in voluptate velit esse cillum dolore eu|in voluptate velit esse cillum dolore eu
   fugiat nulla pariatur. Excepteur sint|fugiat nulla pariatur. Excepteur sint   
occaecat cupidatat non proident, sunt in|occaecat cupidatat non proident, sunt in
  culpa qui officia deserunt mollit anim|culpa qui officia deserunt mollit anim  
                         id est laborum.|id est laborum.                         """

class TestRow(unittest.TestCase):

    def setUp(self):
        self.column: Column = (Column(None)
                .set_h_align('l')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_l: Column = (Column(None)
                .set_h_align('l')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_c: Column = (Column(None)
                .set_h_align('c')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_r: Column = (Column(None)
                .set_h_align('r')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.row_render_1: list[str] = ROW_RENDER_1.split('\n')
        self.row_render_2: list[str] = ROW_RENDER_2.split('\n')

    def test_row_render_1(self):
        row: Row = Row(None, column_separator='|')
        row.columns.append(self.column_r)
        row.columns.append(self.column_l)
        row.build_render_cache()
        render_lines = row.render()
        self.assertEqual(len(self.row_render_1), len(render_lines))

        for line in enumerate(render_lines):
            self.assertEqual(self.row_render_2[line[0]],
                    render_lines[line[0]].line_text)
