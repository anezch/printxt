import unittest
from printxt.v2.model import Column

TEST_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

WRAPPED_TEXT_40_L = """Lorem ipsum dolor sit amet, consectetur 
adipiscing elit, sed do eiusmod tempor  
incididunt ut labore et dolore magna    
aliqua. Ut enim ad minim veniam, quis   
nostrud exercitation ullamco laboris    
nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit  
in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint   
occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim  
id est laborum.                         """

WRAPPED_TEXT_40_R = """ Lorem ipsum dolor sit amet, consectetur
  adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna
   aliqua. Ut enim ad minim veniam, quis
    nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat.
  Duis aute irure dolor in reprehenderit
in voluptate velit esse cillum dolore eu
   fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in
  culpa qui officia deserunt mollit anim
                         id est laborum."""

class TestColumn(unittest.TestCase):
    def setUp(self):
        self.column: Column = (Column(None)
                .set_h_align('l')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_l: Column = (Column(None)
                .set_h_align('l')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_c: Column = (Column(None)
                .set_h_align('c')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.column_r: Column = (Column(None)
                .set_h_align('r')
                .set_v_align('t')
                .set_width(40)
                .set_style_code('b')
                .add_line(TEST_TEXT))
        self.wrapped_40_l = WRAPPED_TEXT_40_L.split('\n')
        self.wrapped_40_r = WRAPPED_TEXT_40_R.split('\n')

    def test_create_column(self):
        self.assertEqual(1, self.column.line_count())
        self.assertEqual(TEST_TEXT, self.column.lines[0])
        self.assertEqual('l', self.column.h_align)
        self.assertEqual('t', self.column.v_align)
        self.assertEqual(40, self.column.width)

    def test_column_wrap_left(self):
        self.column.render_to_cache()

        self.assertEqual(len(self.wrapped_40_l), len(self.column.render_cache))

        for line in enumerate(self.wrapped_40_l):
            self.assertEqual(line[1], self.column.render_cache[line[0]])

    def test_column_wrap_right(self):
        self.column_r.render_to_cache()

        self.assertEqual(len(self.wrapped_40_r), len(self.column_r.render_cache))

        for line in enumerate(self.wrapped_40_r):
            self.assertEqual(line[1], self.column_r.render_cache[line[0]])

    def test_column_v_align_t(self):
        self.column.render_to_cache()

        self.assertEqual(self.column.get_rendered_line(0, vert_space=15),
                self.wrapped_40_l[0])
        self.assertEqual(self.column.get_rendered_line(11, vert_space=15),
                self.wrapped_40_l[11])
        self.assertEqual(self.column.get_rendered_line(12, vert_space=15),
                ' ' * self.column.get_available_width())
        self.assertEqual(self.column.get_rendered_line(14, vert_space=15),
                ' ' * self.column.get_available_width())

    def test_column_v_align_b(self):
        self.column.v_align = 'b'
        self.column.render_to_cache()

        self.assertEqual(self.column.get_rendered_line(3, vert_space=15),
                self.wrapped_40_l[0])
        self.assertEqual(self.column.get_rendered_line(14, vert_space=15),
                self.wrapped_40_l[11])
        self.assertEqual(self.column.get_rendered_line(0, vert_space=15),
                ' ' * self.column.get_available_width())
        self.assertEqual(self.column.get_rendered_line(2, vert_space=15),
                ' ' * self.column.get_available_width())

    def test_column_v_align_c(self):
        self.column.v_align = 'c'
        self.column.render_to_cache()

        self.assertEqual(self.column.get_rendered_line(1, vert_space=15),
                self.wrapped_40_l[0])
        self.assertEqual(self.column.get_rendered_line(12, vert_space=15),
                self.wrapped_40_l[11])
        self.assertEqual(self.column.get_rendered_line(0, vert_space=15),
                ' ' * self.column.get_available_width())
        self.assertEqual(self.column.get_rendered_line(13, vert_space=15),
                ' ' * self.column.get_available_width())

